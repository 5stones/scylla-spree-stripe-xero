## [1.3.1](https://gitlab.com/5stones/scylla-spree-stripe-xero/compare/v1.3.0...v1.3.1) (2019-03-06)


### Bug Fixes

* **ToXeroPaymentTask, ToXeroOverpaymentTask:** Filter out any refunds when creating payments ([6f16d44](https://gitlab.com/5stones/scylla-spree-stripe-xero/commit/6f16d44))



# [1.3.0](https://gitlab.com/5stones/scylla-spree-stripe-xero/compare/v1.2.0...v1.3.0) (2019-03-05)


### Features

* **ToXeroPayment:** Change XeroPayment date to the date of the Payout ([ffce41b](https://gitlab.com/5stones/scylla-spree-stripe-xero/commit/ffce41b))



# [1.2.0](https://gitlab.com/5stones/scylla-spree-stripe-xero/compare/v1.1.0...v1.2.0) (2019-03-01)


### Features

* **ToXeroPaymentTask, ToXeroPayment:** Throw an error if no invoice exists so that there will be a ([b264ff4](https://gitlab.com/5stones/scylla-spree-stripe-xero/commit/b264ff4))



# [1.1.0](https://gitlab.com/5stones/scylla-spree-stripe-xero/compare/v1.0.0...v1.1.0) (2019-02-27)


### Features

* **ToXeroOverpaymentTask, ToXeroOverpaymentAllocationTask:** Add functionality to determine overpay ([cf58528](https://gitlab.com/5stones/scylla-spree-stripe-xero/commit/cf58528))



# [1.0.0](https://gitlab.com/5stones/scylla-spree-stripe-xero/compare/2ea35e4...v1.0.0) (2019-01-18)


### Features

* ***/*:** Add basic module that lines up payments with Spree and pushes them into Xero ([2ea35e4](https://gitlab.com/5stones/scylla-spree-stripe-xero/commit/2ea35e4))



