from scylla import app
from scylla import configuration
import scylla_stripe as stripe
import scylla_xero as xero
from . import conversions
from . import tasks
from pprint import pprint


class App(app.App):

    def _prepare(self):
        self.xero_client = xero.XeroClient()

        stripe_config = configuration.getSection('Stripe')
        spree_config = configuration.getSection('Spree')
        xero_config = configuration.getSection('Xero')

        self.tasks['link-transactions'] = tasks.LinkTransactionsTask(
            (stripe_config.get('name'), 'Transaction'),
            (spree_config.get('name'), 'Payment'),
        )

        self.tasks['payments'] = tasks.ToXeroPaymentTask(
            self.xero_client.payments,
            (stripe_config.get('name'), 'Transaction'),
            conversions.ToXeroPayment(xero_config.get('payment_account')),
            (xero_config.get('name'), 'Payment'),
        )

        self.tasks['overpayments'] = tasks.ToXeroOverpaymentTask(
            getattr(self.xero_client, 'banktransactions'),
            (stripe_config.get('name'), 'Transaction'),
            conversions.ToXeroOverpayment(xero_config.get('overpayment_account')),
            (xero_config.get('name'), 'Overpayment'),
        )

        self.tasks['allocate-overpayments'] = tasks.ToXeroOverpaymentAllocationTask(
            self.xero_client.overpayments,
            (xero_config.get('name'), 'Overpayment'),
            conversions.ToXeroOverpaymentAllocation(),
            (xero_config.get('name'), 'Overpayment'),
            where=(
                '(Allocations.size() = 0 OR Allocations IS NULL) '
                'AND (_is_allocated != true OR _is_allocated IS NULL) '
                'AND $xero_invoice.size() > 0 '
            ),
        ).add_let('$xero_invoice', self._get_invoice_subquery())

    def _get_invoice_subquery(self):
        return  (
            "( "
                "SELECT FROM XeroInvoice "
                "WHERE InvoiceID IN $parent.current.in('Created').in('Created')._parent.out('Created').InvoiceID "
            ") "
        )
