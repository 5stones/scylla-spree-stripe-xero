from scylla import convert
from scylla import configuration
from scylla import orientdb
from scylla import log
import scylla_xero as xero
from dateutil import parser
from pprint import pprint


class ToXeroPayment(xero.XeroConversion):

    def __init__(self, payment_account):
        key_field = 'PaymentID'

        super(ToXeroPayment, self).__init__(key_field)

        self.conversion_map.update({
            'Invoice': (self._get_invoice_id, ),
            'Account': (
                lambda p: { 'Code': payment_account },
            ),
            'Date': ('_parent', 'created', self._get_datetime, ),
            'Amount': ('amount', lambda a : a / 100.0 ),
            'Reference': (self._get_reference, ),
            'IsReconciled': 'false',
            'Status': 'AUTHORISED',
        })

    @staticmethod
    def _get_invoice_id(transaction):
        invoice_id = transaction.get('_InvoiceID', None)
        # create a record error when no invoice is found
        if invoice_id == None:
            raise Exception('No XeroInvoice could be found for the order that generated the payment.')

        return {'InvoiceID': invoice_id }

    @staticmethod
    def _get_datetime(d):
        if isinstance(d, basestring):
            return parser.parse(d)
        else:
            return d

    @staticmethod
    def _get_reference(trans):
        return '{}:{}'.format(trans['_parent']['id'], trans['id'])


class ToXeroOverpayment(xero.XeroConversion):

    def __init__(self, overpayment_account):
        key_field = 'BankTransactionID'

        super(ToXeroOverpayment, self).__init__(key_field)

        self.conversion_map.update({
            'Type': 'RECEIVE-OVERPAYMENT',
            'Reference': ('id', lambda id: 'Stripe ID: {}'.format(id)),
            'Contact': ('customer_details', self._get_contact_id, ),
            'BankAccount': (
                lambda p: { 'Code': overpayment_account },
            ),
            'LineAmountTypes': 'NoTax',
            'LineItems': (self._get_line_item, ),
        })

    @staticmethod
    def _get_contact_id(email):
        contact_id = orientdb.lookup('XeroContact', 'EmailAddress', email, 'ContactID')
        return {
            'ContactID': contact_id
        }

    @staticmethod
    def _get_line_item(transaction):
        return [{
            'Description': 'Overpayment Details: {}'.format(transaction.get('description', '')),
            'LineAmount': (transaction.get('amount', 0.0) / 100.0)
        }]


class ToXeroOverpaymentAllocation(xero.XeroConversion):

    def __init__(self):
        key_field = None

        super(ToXeroOverpaymentAllocation, self).__init__(key_field)

        self.conversion_map.update({
            'OverpaymentID': ('OverpaymentID', ),
            'AppliedAmount': (self._get_amount, ),
            'Invoice': ('$xero_invoice', 0, 'InvoiceID',
                lambda invoice_id: { 'InvoiceID': invoice_id }
            ),
        })

    @staticmethod
    def _get_amount(overpayment):
        amount_due = overpayment.get('$xero_invoice', [])[0].get('AmountDue')
        amount_due = float(amount_due)
        over_amount = float(overpayment.get('Total', 0.0))

        if over_amount > amount_due:
            return str(amount_due)
        else:
            return str(over_amount)
