# coding: utf8
from scylla import tasks
from scylla import orientdb
from scylla import graceful
from scylla import log
from scylla import Record
import scylla_spree as spree
import scylla_xero as xero
import datetime


class LinkTransactionsTask(tasks.ReflectTask):
    """Link Stripe transactions to Xero invoices
    """
    key_field = 'id'

    def __init__(self,
        from_class,
        to_class,
        where=(
            "in('Created').size() = 0 "
            "AND description LIKE 'Spree Order ID: R%' "
        ),
        with_reflection=False
    ):
        super(LinkTransactionsTask, self).__init__(
            from_class,
            to_class,
            where=where,
            with_reflection=with_reflection
        )

        # manually specify task_id to avoid task name collision
        task_id = '{}{} > {}: link'.format(from_class[0], from_class[1], self.to_class)
        self.task_id = task_id

    def _process_response_record(self, obj):
        # extract the spree order number
        r_number = obj.get('description')[16:26]
        p_number = obj.get('description')[27:]
        q = (
            "SELECT @rid.asString(), id "
            "FROM {to_class} "
            "WHERE number = '{p_number}' "
            "AND _parent.number = '{r_number}' "
            "LIMIT 1 "
        ).format(to_class=self.to_class, p_number=p_number, r_number=r_number)

        result = orientdb.execute(q)

        if len(result) == 0:
            print(u'Skipping {} {} as no {} found'.format(self.from_class, obj.get('@rid'), self.to_class))
            return None

        from_rid = result[0].get('rid')
        self._save_response(obj, from_rid)

    def _save_response(self, to_obj, from_rid):
        reflection_type = 'Created'

        # save the response to orientdb
        with graceful.Uninterruptable():

            # draw the edge
            orientdb.create_edge(from_rid, to_obj['@rid'] , reflection_type, content=None)
            orientdb.execute('UPDATE {} SET _updated_at=date()'.format(to_obj['@rid']))
            print(u'{} {}  –{}→  {} {}'.format(
                from_rid,
                self.to_class,
                reflection_type,
                to_obj['@class'],
                to_obj['@rid'],
            ))


class ToXeroPaymentTask(xero.ToXeroTask):
    """Push payments that line up with Spree charges into Xero"""
    projections = '*, first(in(\'Created\')._parent.out(\'Created\').InvoiceID) as _InvoiceID'


    def __init__(self,
            xero_client_collection,
            from_class,
            conversion,
            to_class,
            where=(
                "type = 'charge' "
                "AND first(in('Created')._parent) IS NOT NULL "
                "AND (_is_overpayment != true OR _is_overpayment IS NULL) "
            ),
            with_reflection=False
    ):

        super(ToXeroPaymentTask, self).__init__(
            xero_client_collection,
            from_class,
            conversion,
            to_class,
            where=where,
            with_reflection=with_reflection)

    def _handle_response_error(self, result, from_obj, request, is_update):
        # this record failed; format and throw exception
        errors = [error['Message'] for error in result['ValidationErrors']]

        # mark the transaction as an overpayment to be handled by another sync
        if self._is_overpayment_error(errors):
            q = (
                'UPDATE {} '
                'SET _is_overpayment = true, _updated_at=date() '
                'WHERE @rid = {} '
            ).format(from_obj['@class'], from_obj['@rid'])

            orientdb.execute(q);
        else:
            err = Exception(errors)
            err.details = result
            raise err

    def _is_overpayment_error(self, errors):
        return len(errors) == 1 and errors[0] == 'Payment amount exceeds the amount outstanding on this document'


class ToXeroOverpaymentTask(xero.ToXeroTask):
    """Push overpayments that line up with Spree charges into Xero"""
    projections = '*, first(in(\'Created\')._parent.out(\'Created\').InvoiceID) as _InvoiceID'

    def __init__(self,
            xero_client_collection,
            from_class,
            conversion,
            to_class,
            where=(
                "type = 'charge' "
                "AND first(in('Created')._parent.out('Created').InvoiceID) IS NOT NULL "
                "AND _is_overpayment = true "
            ),
            with_reflection=False
    ):

        super(ToXeroOverpaymentTask, self).__init__(
            xero_client_collection,
            from_class,
            conversion,
            to_class,
            where=where,
            with_reflection=with_reflection)

    def _handle_response_success(self, result, from_obj, request, is_update):
        # this record succeeded; save the response and link it
        # verify we always use an 'Overpayment'
        result_rec = xero.records.XeroRecord.factory(result, obj_type='Overpayment')
        self._save_response(from_obj, result_rec, is_update=is_update, request=request)


class ToXeroOverpaymentAllocationTask(xero.ToXeroTask):
    """Push overpayments allocations that line up with Spree charges into Xero"""
    # projections = '*, first(in(\'Created\')._parent.out(\'Created\').InvoiceID) as _InvoiceID'
    key_field = 'OverpaymentID'

    def __init__(self,
            xero_client_collection,
            from_class,
            conversion,
            to_class,
            where=None,
            with_reflection=False
    ):

        super(ToXeroOverpaymentAllocationTask, self).__init__(
            xero_client_collection,
            from_class,
            conversion,
            to_class,
            where=where,
            with_reflection=with_reflection)

    def _create_record(self, obj):
        rec = Record(obj)
        rec.key_field = self.key_field
        return rec

    def _process_response(self, from_objs):
        """Processes a page of records from orient, and pushes them to Xero.
        """
        if not from_objs or not len(from_objs):
            print('No updated records')
            return

        results = [self._create_record(obj) for obj in from_objs]
        return super(ToXeroOverpaymentAllocationTask, self)._process_response(results)

    def _process_response_record(self, obj):
        """Creates objs to pass to Xero which will create or update those records.
        """
        data = self.conversion(obj)
        invoice_id = data.pop('OverpaymentID', None)

        # save the page of objects to xero
        result = self._allocate_overpayment(invoice_id, [data])
        self._save_one_response(result, obj, data)

    def _save_one_response(self, result, from_obj, request):
        """Here we take the newly created allocation and populate it back
            on the original overpayment object and save it.
        """
        # catch errors while processing each record result
        with log.ErrorLogging(self.task_id, from_obj):
            is_update = True
            # this record succeeded; save the response and link it
            result = self._fix_datetime(result)
            from_obj_data = from_obj.data
            from_obj_data['Allocations'] = result
            from_obj_data['_is_allocated'] = True
            result_rec = xero.records.XeroRecord.factory(from_obj_data, obj_type='Overpayment')
            self._save_response(from_obj, result_rec, is_update=is_update, request=request)

    def _fix_datetime(self, results):
        fixed = []
        for r in results:
            if 'Date' in r:
                r['Date'] = datetime.datetime.now()
            fixed.append(r)

        return fixed

    # dont use this method for overpayment allocations
    def _save_page(self, from_objs, to_objs):
        pass

    # taken from: https://github.com/freakboy3742/pyxero/issues/99#issuecomment-281069051
    def _allocate_overpayment(self, invoice_id, allocations):
        """
        Must pass in xero as it needs credentials if using public method.

        allocations should be an array of dictionaries containing amount, invoice:invoice idXero GUIDs for the contacts.

        The request should look like:
        PUT Overpayments/a1665e41-719b-400d-ae59-bc92655d8366/Allocations
        <Allocations>
            <Allocation>
                <AppliedAmount>60.50</AppliedAmount>
                <Invoice>
                    <InvoiceID>f5832195-5cd3-4660-ad3f-b73d9c64f263</InvoiceID>
                </Invoice>
            </Allocation>
        </Allocations>
        """

        result = None
        try:
            # Store the original endpoint base_url
            old_base_url = self.xero_client_collection.base_url
            old_name = self.xero_client_collection.name
            old_singular = self.xero_client_collection.singular
            result = None

            # Call the API
            self.xero_client_collection.base_url = '{}/Overpayments/{}'.format(old_base_url, invoice_id)
            self.xero_client_collection.name = 'Allocations'
            self.xero_client_collection.singular = 'Allocation'

            result = self.xero_client_collection.put(allocations)
        except Exception as e:
            raise e
        finally:
            # Reset the base_url
            self.xero_client_collection.base_url = old_base_url
            self.xero_client_collection.name = old_name
            self.xero_client_collection.singular = old_singular

        return result
