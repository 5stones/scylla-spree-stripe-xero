from distutils.core import setup
import json

with open('package.json', 'r') as f:
    package_json = json.load(f)

version = package_json['version']

setup(
    name = 'scylla-spree-stripe-xero',
    packages = ['scylla_spree_stripe_xero'],
    version = version,

    description = 'Scylla-Spree-Stripe-Xero provides the basic utilities to integrate Stripe with the standard Xero API',

    #author = '',
    #author_email = '',

    url = 'https://git@gitlab.com:5stones/scylla-spree-stripe-xero',
    download_url = 'https://gitlab.com/5stones/scylla-spree-stripe-xero/repository/archive.tar.gz?ref=' + version,

    keywords = 'integration scylla xero stripe',

    classifiers=[
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',

        #'Intended Audience :: Developers',
        #'Topic :: Software Development :: Build Tools',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],

    install_requires = [
        'requests',
        'scylla',
        'scylla_stripe',
        'scylla_xero',
        'python-dateutil',
    ],
    dependency_links=[
        'git+https://gitlab.com/5stones/scylla.git',
        'git+https://gitlab.com/5stones/scylla-stripe.git',
        'git+https://gitlab.com/5stones/scylla-xero.git',
    ],
)
